/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PERSISTENCE;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import DATA.data;
import java.awt.List;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author root
 */
public class MemorySlot {
 
    
    public void write(data d){
  
        try(FileWriter fw = new FileWriter("MemoryCard.data", true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw))
        {
            out.println(d.getName()+"#"+d.getTime());
            out.close();
        } catch (IOException e) {
        
        }            
    }
    
    public data[] read(){
        FileReader fileReader = null;
        data players[];
        try {
            File file = new File("MemoryCard.data");
            fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            ArrayList<String> names = new ArrayList<String>();
            
            while ((line = bufferedReader.readLine()) != null) {
                names.add(line);
            }
            players=new data[names.size()];
            
            String buffer[]=new String[2];
            for (int i = 0; i < players.length; i++) {
                buffer=names.get(i).split("#");
                players[i]=new data(buffer[0],Long.parseLong(buffer[1]));
            }
            
            return players;
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MemorySlot.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MemorySlot.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fileReader.close();
            } catch (IOException ex) {
                Logger.getLogger(MemorySlot.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }
            
    
}
