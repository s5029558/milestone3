package minesweeper;

import DATA.data;
import PERSISTENCE.MemorySlot;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import javax.swing.ImageIcon;
import javax.swing.JButton;        
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.Timer;

public class ColorWeeper extends JFrame implements ActionListener{
    
    private String PLAYER_NAME="";
    final int MINE=10;
    final int NUM_IMAGES=4;
    static JFrame frame= new JFrame("ColorWeeper");
    JButton start=new JButton();
    
    JButton stop=new JButton();
    int N_ROWS=5,N_COL=5;
    int COLORS=0;
    JButton[][] buttons=new JButton[N_ROWS][N_COL];
    String[][] counts=new String[N_ROWS][N_COL];
    Container grid= new Container();
    Container grid2= new Container();
    String[][] last=new String[1][1];
    Image[] img = new Image[NUM_IMAGES];
    JLabel time=new JLabel("TIME: ");
    int lastTIME=0;
    static Timer t;
    
    public ColorWeeper(){
        
        PLAYER_NAME=JOptionPane.showInputDialog(null,"Enter your Name: ");

        COLORS=Integer.parseInt(JOptionPane.showInputDialog("Colors Repeat: "));


        for (int i = 0; i < NUM_IMAGES; i++) {
            img[i] = (new ImageIcon(getClass().getResource("/COLORS/" +i + ".png"))).getImage();
            
            
        }

        frame.setSize(1000,800);
        frame.setLayout(new BorderLayout());
        
        grid2.setLayout(new BorderLayout());
       
        start.setIcon(new ImageIcon(img[3]));
        start.addActionListener(this);
        grid2.add(start,BorderLayout.NORTH);
        
        
        frame.add(grid2,BorderLayout.SOUTH);
        
        grid.setLayout(new GridLayout(N_ROWS, N_COL));
        
        for (int i=0; i<buttons.length;i++){
            for(int j=0;j<buttons.length;j++){
                buttons[i][j]=new JButton();
                buttons[i][j].addActionListener(this);
                buttons[i][j].setEnabled(false);
                //buttons[i][j].setBackground(Color.black);
                grid.add(buttons[i][j]);
            }
        }
        
        frame.add(grid,BorderLayout.CENTER);
        frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        frame.setVisible(true);
        addColors();
        
    }
    
    public static void main(String[] args) {
        new ColorWeeper();
 
    }

    public void win(){
        int c=0;
        for (int i = 0; i <counts.length; i++) {
            for (int j = 0; j < counts[0].length; j++) {
                if(buttons[i][j].isEnabled()== false){
                    c++;
                }
            }
        }
        if(c==((N_ROWS*N_COL) -COLORS)){
            JOptionPane.showMessageDialog(null, "YOU WIN !!!!");
            t.stop();
            data player=new data(PLAYER_NAME, lastTIME);
            MemorySlot memory=new MemorySlot();
            memory.write(player);
            
            for(int x=0;x<buttons.length;x++){
                for (int y = 0; y < buttons[0].length; y++) {
                    if(buttons[x][y].isEnabled()){                        
                             if(counts[x][y]=="BLUE"){
                                 buttons[x][y].setIcon(new ImageIcon(img[1]));
                                 buttons[x][y].setEnabled(false);
                                 buttons[x][y].setDisabledIcon(new ImageIcon(img[1]));
                             }
                             if(counts[x][y]=="AMARILLO"){
                                 buttons[x][y].setIcon(new ImageIcon(img[0]));
                                 buttons[x][y].setEnabled(false);
                                 buttons[x][y].setDisabledIcon(new ImageIcon(img[0]));
                             }
                             if(counts[x][y]=="ROJO"){
                                 buttons[x][y].setIcon(new ImageIcon(img[2]));
                                 buttons[x][y].setEnabled(false); 
                                 buttons[x][y].setDisabledIcon(new ImageIcon(img[2]));

                             }
                             if(counts[x][y]==null){
                                 buttons[x][y].setEnabled(false);
                             }
                    }
                }
            }
        }
        
       // colors
    }
    public void lostGame(){
        for(int x=0;x<buttons.length;x++){
            for (int y = 0; y < buttons[0].length; y++) {
                if(buttons[x][y].isEnabled()){
                    
                    if(counts[x][y]=="BLUE"){
                        buttons[x][y].setIcon(new ImageIcon(img[1]));
                        buttons[x][y].setEnabled(false);
                        buttons[x][y].setDisabledIcon(new ImageIcon(img[1]));
                    }
                    if(counts[x][y]=="AMARILLO"){
                        buttons[x][y].setIcon(new ImageIcon(img[0]));
                        buttons[x][y].setEnabled(false);
                        buttons[x][y].setDisabledIcon(new ImageIcon(img[0]));
                    }
                    if(counts[x][y]=="ROJO"){
                        buttons[x][y].setIcon(new ImageIcon(img[2]));
                        buttons[x][y].setEnabled(false); 
                        buttons[x][y].setDisabledIcon(new ImageIcon(img[2]));

                    }
                    if(counts[x][y]==null){
                        buttons[x][y].setEnabled(false);
                    }
                }
            }
        }
        t.stop();
    }
    public void addColors() {
        for (int x = 0; x < COLORS; x++) {
                    for (int i = 0; i < 3; i++){
                        
                            if(i==0){
                            int choiceX=(int)(Math.random()*(N_COL));
                            int choiceY=(int)(Math.random()*(N_COL));                        
                            counts[choiceX][choiceY]="BLUE";
                            }
                            
                            if(i==1){
                            int choiceX=(int)(Math.random()*(N_COL));
                            int choiceY=(int)(Math.random()*(N_COL));                        
                            counts[choiceX][choiceY]="AMARILLO";}
                            
                            if(i==2){
                            int choiceX=(int)(Math.random()*(N_COL));
                            int choiceY=(int)(Math.random()*(N_COL));                        
                            counts[choiceX][choiceY]="ROJO";}
                        
                    }
            }
        
    }
    

    
    @Override
    public void actionPerformed(ActionEvent event) {
        if(event.getSource().equals(t)){
                    lastTIME=lastTIME+1;
                    frame.setTitle("ColorSweeper TIME: "+lastTIME+" SEG");
        }
            
        if(event.getSource().equals(start)){
            lastTIME=0;
            last[0][0]=null;
            
                for(int x=0;x<buttons.length;x++){
                    for (int y = 0; y < buttons[0].length; y++) {

                            if(counts[x][y]=="BLUE"){
                                buttons[x][y].setEnabled(true);
                                buttons[x][y].setIcon(null);
                            }
                            if(counts[x][y]=="AMARILLO"){                                
                                buttons[x][y].setEnabled(true);                                
                                buttons[x][y].setIcon(null);
                            }
                            if(counts[x][y]=="ROJO"){
                                buttons[x][y].setEnabled(true); 
                                buttons[x][y].setIcon(null);

                            }
                            if(counts[x][y]==null){
                                buttons[x][y].setEnabled(true);
                                buttons[x][y].setIcon(null);
                            }
                        
                    }
                }

            
            lastTIME=0;
            t=new Timer(1000, this);
            addColors();
            t.start();
            
            
        }

        
        else{
            for (int x = 0; x < buttons.length; x++) { 
                 for (int y = 0; y < buttons.length; y++) {
                     if(event.getSource().equals(buttons[x][y])){
                         if(counts[x][y]=="BLUE"){
                            if(last[0][0]=="BLUE") {JOptionPane.showMessageDialog(null,"YOU LOSE...");lostGame();return;}
                                buttons[x][y].setIcon(new ImageIcon(img[1]));
                                buttons[x][y].setDisabledIcon(new ImageIcon(img[1]));
                                buttons[x][y].setEnabled(false);                                
                                last[0][0]="BLUE";
                         }
                                                                                                                                                                                                                    
                         if(counts[x][y]=="AMARILLO"){
                            if(last[0][0]=="AMARILLO") {JOptionPane.showMessageDialog(null,"YOU LOSE...");lostGame();return;}
                                buttons[x][y].setIcon(new ImageIcon(img[0]));
                                buttons[x][y].setDisabledIcon(new ImageIcon(img[0]));
                                buttons[x][y].setEnabled(false);
                                last[0][0]="AMARILLO";
                         }
                         if(counts[x][y]=="ROJO"){
                            if(last[0][0]=="ROJO") {JOptionPane.showMessageDialog(null,"YOU LOSE...");lostGame();return;}
                            buttons[x][y].setIcon(new ImageIcon(img[2]));
                            buttons[x][y].setDisabledIcon(new ImageIcon(img[2]));
                            buttons[x][y].setEnabled(false);
                            last[0][0]="ROJO";
                         }
                         if(counts[x][y]==null){
                            buttons[x][y].setEnabled(false);
                         }
                     }
                    
                }
            }
            
            win();
        }
    }
    
    
}