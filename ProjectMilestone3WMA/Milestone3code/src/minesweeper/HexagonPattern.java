package minesweeper;

import DATA.data;
import PERSISTENCE.MemorySlot;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import javax.swing.*;
// class hexagon 
public class HexagonPattern extends JFrame implements ActionListener{
    private static  long serialVersionUID = 1L;
    private static  int ROWS = 5;
    private static  int COLUMNS = 5;
    private static  int N_MINES = 3;
    private final int NUM_IMAGES=13;
    private String PLAYER_NAME="";
    int lastTIME=0;
    final int MINE=10;
    static Timer t;
    JButton start=new JButton();
    
    private static final HexagonButton[][] buttons=new HexagonButton[ROWS][COLUMNS];
    
    private Image[] img = new Image[NUM_IMAGES+1];
    int[][] counts=new int[ROWS][ROWS];
    Container grid= new Container();
    
    public HexagonPattern() {
        initGUI();
      
        
    }


    public void initGUI() {
        
        PLAYER_NAME=JOptionPane.showInputDialog(null,"Enter your Name: ");

        this.setTitle("Hexagonal MinesWeeper");
        N_MINES=Integer.parseInt(JOptionPane.showInputDialog("Mines in the Field: "));
        
        int offsetX = -10;
        int offsetY = 0;
        
        for (int i = 0; i <= NUM_IMAGES; i++) {
            img[i] = (new ImageIcon( getClass().getResource("/ImagesHEX/" +i + ".png"))).getImage();
            
            
        }
        setLayout(new BorderLayout());
        grid.setLayout(null);
        
        Container grid1= new Container();
        grid1.setLayout(new BorderLayout());
        
        Image img2 = (new ImageIcon(getClass().getResource("/COLORS/3.png"))).getImage();
        start.setIcon(new ImageIcon(img2));
        start.addActionListener(this);
        
        grid1.add (start,BorderLayout.SOUTH);        
        add(grid1,BorderLayout.SOUTH);
        
        for(int row = 0; row < ROWS; row++) {
            for(int col = 0; col < COLUMNS; col++){
                buttons[row][col] = new HexagonButton(row, col);
                buttons[row][col].setIcon(new ImageIcon(img[0]));    
                buttons[row][col].addActionListener(this);
                buttons[row][col].setEnabled(false);
                add(buttons[row][col]);
                buttons[row][col].setBounds(offsetY, offsetX, 105, 95);
                offsetX += 87;
                grid.add(buttons[row][col]);
            }
            if(row%2 == 0) {
                offsetX = -52;
            } else {
                offsetX = -10;
            }
            offsetY += 76;
        }
        
        add(grid,BorderLayout.CENTER);
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setLocation(new Point(400, 300));
        setSize( 415, 515);
        setResizable(true);
        setVisible(true);
        
                  
        createRandomMines();
        
        
        
        
    }
    
    public void win(){
        int c=0;
        for (int i = 0; i <counts.length; i++) {
            for (int j = 0; j < counts[0].length; j++) {
                if(buttons[i][j].isEnabled()== false){
                    c++;
                }
            }
        }
        if(c==((ROWS*COLUMNS) - N_MINES -1)){
            JOptionPane.showMessageDialog(null, "YOU WIN !!!!");
            t.stop();
            data player=new data(PLAYER_NAME, lastTIME);
            MemorySlot memory=new MemorySlot();
            memory.write(player);
            
            for(int x=0;x<buttons.length;x++){
                for (int y = 0; y < buttons[0].length; y++) {
                    if(buttons[x][y].isEnabled()){
                        if(counts[x][y]!=MINE){
                            buttons[x][y].setDisabledIcon(new ImageIcon(img[counts[x][y]]));
                            buttons[x][y].setEnabled(false);
                        }
                        else{
                            buttons[x][y].setEnabled(false);
                            buttons[x][y].setIcon(new ImageIcon(img[13]));    
                            buttons[x][y].setDisabledIcon(new ImageIcon(img[13]));
                        }

                    }
                }
            }
        }
        
        
    }
    

    
    @Override
    public void actionPerformed(ActionEvent event) {
        
        if(event.getSource().equals(t)){
            lastTIME=lastTIME+1;
            this.setTitle("HexaSweeper TIME: "+lastTIME+" SEG");
        }
            
        int c=0;
        for (int i = 0; i <counts.length; i++) {
            for (int j = 0; j < counts[0].length; j++) {
                if(buttons[i][j].isEnabled()== false){
                    c++;
                }
            }
        }
               
        if(event.getSource().equals(start)){
            for (int i = 0; i < buttons.length; i++) {
                for (int j = 0; j < buttons.length; j++) {
                    buttons[i][j].setIcon(new ImageIcon(img[10]));    
                    buttons[i][j].setEnabled(true);
                }
            }
            lastTIME=0;
            t=new Timer(1000, this);
            t.start();
            createRandomMines();
        }
        else{
            for (int x = 0; x < buttons.length; x++) { 
                 for (int y = 0; y < buttons.length; y++) {
                     if(event.getSource().equals(buttons[x][y])){
                         
                         if(counts[x][y]==MINE){
                            lostGame();
                         }
                         else if(counts[x][y]==0){ 
                            buttons[x][y].setDisabledIcon(new ImageIcon(img[counts[x][y]]));
                            buttons[x][y].setEnabled(false);
                            ArrayList<Integer> toClear =new ArrayList<Integer>();
                            toClear.add(x*100+y);
                            clearZeros(toClear); 
                         }
                         else{
                            buttons[x][y].setDisabledIcon(new ImageIcon(img[counts[x][y]]));
                            buttons[x][y].setEnabled(false);
                         }
                         
                     }
                    
                }
            }
            win();
        }
    }
    

    public static void main(String[] args) {
        HexagonPattern hexPattern = new HexagonPattern();
     
    }
    public void lostGame(){
        for(int x=0;x<buttons.length;x++){
            for (int y = 0; y < buttons[0].length; y++) {
                if(buttons[x][y].isEnabled()){
                    if(counts[x][y]!=MINE){
                        buttons[x][y].setDisabledIcon(new ImageIcon(img[counts[x][y]]));
                        buttons[x][y].setEnabled(false);
                    }
                    else{
                        buttons[x][y].setEnabled(false);
                        buttons[x][y].setIcon(new ImageIcon(img[9]));    
                        buttons[x][y].setDisabledIcon(new ImageIcon(img[9]));
                    }
                        
                }
            }
        }
        JOptionPane.showMessageDialog(null, "YOU LOSE");
        t.stop();
    }

    public void createRandomMines() {
        ArrayList<Integer> list = new ArrayList<Integer>();
        for (int x = 0; x < counts.length; x++) {
            for (int y = 0; y < counts[0].length; y++) {
                list.add(x*100+y);
                
            }
        }
        counts=new int[ROWS][COLUMNS];
        for (int i = 0; i < N_MINES; i++) {
            int choice=(int)(Math.random()*list.size());
            counts[ list.get(choice)/100 ][list.get(choice)%100]=MINE;
            list.remove(choice);
        }
        
        
        for (int x = 0; x < counts.length; x++) {
            for (int y = 0; y <counts[0].length; y++) {
                if(counts[x][y]!=MINE){
                    
                    int neighborcount=0;
                    if(x>0 && y>0 && counts[x-1][y-1]==MINE){// UP LEFT
                        neighborcount++;
                    }
                    
                    if(y>0 && counts[x][y-1]==MINE){//UP
                        neighborcount++;
                    }
                    
                    if(x<counts.length-1 && y>0 && counts[x+1][y-1]==MINE){// UP RIGHT
                        neighborcount++;
                    }
                                 
                    
                    if(x>0 && y<counts[0].length-1 && counts[x-1][y+1]==MINE){// DOWN LEFT
                        neighborcount++;
                    } 

                    if(y<counts[0].length-1 && counts[x][y+1]==MINE){// DOWN
                        neighborcount++;
                    }
                    if(x<counts.length -1 && y < counts[0].length -1 && counts[x+1][y+1]==MINE){//DOWN RIGHT
                        neighborcount++;
                    }

                    counts[x][y]=neighborcount;
                }
            }
            
        }
    }

 public void clearZeros(ArrayList<Integer> toClear){
        
        if (toClear.isEmpty()){return;}
        
        else{
            int x = toClear.get(0)/100;
            int y = toClear.get(0)%100;
            toClear.remove(0);
            
            if(counts[x][y]==0){
                
                if(x>0 && y>0 && buttons[x-1][y-1].isEnabled()){// UP LEFT                  
                    if (counts[x-1][y-1]==0) {
                        buttons[x-1][y-1].setIcon(new ImageIcon(img[counts[x-1][y-1]]));      
                        toClear.add( (x-1)*100+(y-1) );
                        buttons[x-1][y-1].setEnabled(false);
                        buttons[x-1][y-1].setDisabledIcon(new ImageIcon(img[counts[x-1][y-1]]));

                    }
                }
                  
                if(y>0){// UP
                    if (counts[x][y-1]==0 && buttons[x][y-1].isEnabled()) {
                        buttons[x][y-1].setIcon(new ImageIcon(img[counts[x][y-1]]));        
                        toClear.add(x*100+(y-1));
                        buttons[x][y-1].setEnabled(false);
                        buttons[x][y-1].setDisabledIcon(new ImageIcon(img[counts[x][y-1]]));
                    }
                }
                
                if(x<counts.length-1 && y>0 && buttons[x+1][y-1].isEnabled() ){// UP RIGHT
                    if (counts[x+1][y-1]==0) {
                        buttons[x+1][y-1].setIcon(new ImageIcon(img[counts[x+1][y-1]]));    
                        toClear.add((x+1)*100+(y-1));
                        buttons[x+1][y-1].setEnabled(false);
                        buttons[x+1][y-1].setDisabledIcon(new ImageIcon(img[counts[x+1][y-1]]));
                    }
                }
            
                if(x>0 && y<counts[0].length-1 && buttons[x-1][y+1].isEnabled() ){// DOWN LEFT                        
                    if (counts[x-1][y+1]==0) {
                        buttons[x-1][y+1].setIcon(new ImageIcon(img[counts[x-1][y+1]]));
                        toClear.add((x-1)*100+(y+1));
                        buttons[x-1][y+1].setEnabled(false);
                        buttons[x-1][y+1].setDisabledIcon(new ImageIcon(img[counts[x-1][y+1]]));
                    }
                } 
                
                if(y<counts[0].length-1 && buttons[x][y+1].isEnabled()){// DOWN                    
                    if (counts[x][y+1]==0) {
                        buttons[x][y+1].setIcon(new ImageIcon(img[counts[x][y+1]]));    
                        toClear.add(x*100+(y+1));
                        buttons[x][y+1].setEnabled(false);
                        buttons[x][y+1].setDisabledIcon(new ImageIcon(img[counts[x][y+1]]));
                    }
                }
                if(x<counts.length-1 && y<counts[0].length-1 && buttons[x+1][y+1].isEnabled()){// DOWN RIGHT    
                    if (counts[x+1][y+1]==0) {
                        buttons[x+1][y+1].setIcon(new ImageIcon(img[counts[x+1][y+1]]));
                        toClear.add((x+1) *100 + (y+1));
                        buttons[x+1][y+1].setEnabled(false);
                        buttons[x+1][y+1].setDisabledIcon(new ImageIcon(img[counts[x+1][y+1]]));
                    }
                }
                
              
                
                
            }
            clearZeros(toClear);
            
        }
        
    }
    
    
    
    
    
    
    //Following class draws the Buttons
    class HexagonButton extends JButton {
        private static final long serialVersionUID = 1L;
        private static final int SIDES = 6;
        private static final int SIDE_LENGTH = 50;
        public static final int LENGTH = 95;
        public static final int WIDTH = 105;
        private int row = 0;
        private int col = 0;

        public HexagonButton(int row, int col) {
            setContentAreaFilled(false);
            setFocusPainted(true);
            setBorderPainted(false);
            setPreferredSize(new Dimension(WIDTH, LENGTH));
            this.row = row;
            this.col = col;
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            Polygon hex = new Polygon();
            for (int i = 0; i < SIDES; i++) {
                hex.addPoint((int) (50 + SIDE_LENGTH * Math.cos(i * 2 * Math.PI / SIDES)), //calculation for side
                        (int) (50 + SIDE_LENGTH * Math.sin(i * 2 * Math.PI / SIDES)));   //calculation for side
            }       
            g.drawPolygon(hex);
        }

        public int getRow() {
            return row;
        }

        public int getCol() {
            return col;
        }
    }
}
