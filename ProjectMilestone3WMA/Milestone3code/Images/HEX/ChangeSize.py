# resize an image using the PIL image library
# free from:  http://www.pythonware.com/products/pil/index.htm
# tested with Python24        vegaseat     11oct2005
from PIL import Image
import os,sys
# open an image file (.bmp,.jpg,.png,.gif) you have in the working folder

if (len(sys.argv)==1):
    print("NO ARGUMENTS") 
    exit(0)
    
imageFile = sys.argv[1:]


for i in range(0,len(imageFile)):    
    im1 = Image.open(imageFile[i])
    # adjust width and height to your needs
    width = 40
    height = 40
    # use one of these filter options to resize the image
    im2 = im1.resize((width, height), Image.NEAREST)      # use nearest neighbour
    ext = ".png"
    im2.save("new/"+str(i)+ ext)
    
    
